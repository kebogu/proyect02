package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class LongComparator implements Comparator<Long>{

	public int compare(Long o1, Long o2) {
		// TODO Auto-generated method stub
		if(o1 < o2)
			return -1;
		else if (o1 > o2)
			return 1;
		return 0;
	}
	
}

