package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

public class CarTable {

	private static HashTableOA<Long, Car> instance = new HashTableOA<Long, Car>(97, new LongComparator(), new CarComparator());

	public static HashTableOA<Long,Car> getInstance(){
		return instance;
	}
	
	public static void resetCars() {
		instance.clear();
	}
}


