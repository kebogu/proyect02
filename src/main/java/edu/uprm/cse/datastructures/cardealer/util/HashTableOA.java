package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;

import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.model.LongComparator;

public class HashTableOA<K, V> implements Map<K, V> {

	public static class MapEntry<K, V>{
		private K key;
		private V value;
		public K getKey()
		{
			return key;
		}
		public void setKey(K key)
		{
			this.key = key;
		}
		public V getValue()
		{
			return value;
		}
		public void setValue(V value)
		{
			this.value = value;
		}
		public MapEntry(K key, V value)
		{
			super();
			this.key = key;
			this.value = value;
		}
	}
	
	private int currentSize;
	private MapEntry<K,V>[] tableOfEntries;
	private Boolean[] isAvailable;
	private static final int DEFAULT_BUCKETS = 97;
	private Comparator<K> keyComp;
	private Comparator<V> valueComp;
	
	private int hashFunction1(K key) {
		return key.hashCode() % this.tableOfEntries.length;
	}
	
	private int hashFunction2(K key) {
		return (key.hashCode() + 25) % this.tableOfEntries.length;
	}
	
	public HashTableOA(int numBuckets, Comparator<K> keyComp, Comparator<V> valueComp)
	{
		 this.keyComp = keyComp;
		 this.valueComp = valueComp;
		this.currentSize = 0;
		this.tableOfEntries = new MapEntry[numBuckets];
		this.isAvailable = new Boolean[numBuckets];
		for (int i = 0; i < numBuckets; i++) {
			this.tableOfEntries[i] = new MapEntry<K,V>(null, null);
			this.isAvailable[i] = true;
		}
	}
	
	public HashTableOA(Comparator<K> keyComp, Comparator<V> valueComp)
	{
		
		this(DEFAULT_BUCKETS, keyComp, valueComp);
	}
	
	public HashTableOA()
	{

		 this.keyComp = keyComp;
		 this.valueComp = valueComp;
		this.currentSize = 0;
		this.tableOfEntries =  new MapEntry[DEFAULT_BUCKETS];
		this.isAvailable = new Boolean[DEFAULT_BUCKETS];
		for (int i = 0; i < DEFAULT_BUCKETS; i++) {
			this.tableOfEntries[i] = new MapEntry<K,V>(null, null);
			this.isAvailable[i] = true;
		}
	
		
	}
	


	@Override
	public int size() {
		// TODO Auto-generated method stub
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return this.size() == 0;
	}

	@Override
	public V get(K key) {
		// TODO Auto-generated method stub
		int givenPosition1 = this.hashFunction1(key);
		
		if(tableOfEntries[givenPosition1] != null)
		{
			if(!isAvailable[givenPosition1] && tableOfEntries[givenPosition1].key.equals(key))
			{
				return tableOfEntries[givenPosition1].value;
			}
		}
		
		int givenPosition2 = this.hashFunction2(key);
		
		if(tableOfEntries[givenPosition2] != null)
		{
		if(!isAvailable[givenPosition2] && tableOfEntries[givenPosition2].key.equals(key))
		{
			return tableOfEntries[givenPosition2].value;
		}
		}
		
		for (int i = 0; i < tableOfEntries.length; i++) {
			if(tableOfEntries[i] != null)
			{
			if(!isAvailable[i] && tableOfEntries[i].key.equals(key))
			{
				return tableOfEntries[i].value;
			}
			}
		}
		return null;
		
		
	}

	@Override
	public V put(K key, V value) {
		// TODO Auto-generated method stub
		reallocate();
		
		int givenPos1 = this.hashFunction1(key);
		
		if(this.isAvailable[givenPos1])
		{
			this.adder(givenPos1, key, value);
			return null;
		}
		
		else if(this.tableOfEntries[givenPos1].getKey().equals(key) )
		{
			return this.replacer(givenPos1, key, value);
		}
		else 
		{
			int givenPos2 = this.hashFunction2(key);
			if(this.isAvailable[givenPos2])
			{
				this.adder(givenPos2, key, value);
				return null;
			}
			
			else if(this.tableOfEntries[givenPos2].getKey().equals(key) )
			{
				return this.replacer(givenPos2, key, value);
			}
			
			else
			{
				for (int i = 0; i < this.tableOfEntries.length - 1; i++) 
				{
					if(this.isAvailable[i])
					{
						this.adder(i, key, value);
						return null;
					}
					else if(this.tableOfEntries[i].getKey().equals(key))
					{
						return this.replacer(i, key, value);
					}
				}
				
				
			}
		}
		return null;
	}

	
	
	private void adder(int position, K key, V value)
	{
		this.tableOfEntries[position] = new MapEntry<K, V>(key, value);
		this.isAvailable[position] = false;
		this.currentSize++;
	}
	
	private V replacer(int position, K key, V value)
	{
		V oldValue = this.tableOfEntries[position].getValue();
		this.tableOfEntries[position] = new MapEntry<K, V>(key, value);
		return oldValue;
	}
	@Override
	public V remove(K key) {
		int givenPosition1 = this.hashFunction1(key);
		if(tableOfEntries[givenPosition1] != null)
		{
		if(!isAvailable[givenPosition1] && tableOfEntries[givenPosition1].key.equals(key))
		{
			isAvailable[givenPosition1] = true;
			this.currentSize--;
			return tableOfEntries[givenPosition1].value;
		}
		}
	
	int givenPosition2 = this.hashFunction2(key);
	if(tableOfEntries[givenPosition2] != null)
	{
	if(!isAvailable[givenPosition2] && tableOfEntries[givenPosition2].key.equals(key))
	{
		isAvailable[givenPosition1] = true;
		this.currentSize--;
		return tableOfEntries[givenPosition2].value;
	}
	}
	for (int i = 0; i < tableOfEntries.length; i++) {
		if(tableOfEntries[i] != null)
		{
		if(!isAvailable[i] && tableOfEntries[i].key.equals(key))
		{
			isAvailable[givenPosition1] = true;
			this.currentSize--;
			return tableOfEntries[i].value;
		}
		}
	}
	return null;
	}
	
	 

	@Override
	public boolean contains(K key) {
		// TODO Auto-generated method stub
		
		return this.get(key) != null;
	}

	@Override
	public SortedList<K> getKeys() {
		// TODO Auto-generated method stub
	CircularSortedDoublyLinkedList<K> list = new CircularSortedDoublyLinkedList<K>(keyComp);
	for (int i = 0; i < tableOfEntries.length; i++) {
		if(!isAvailable[i])
		{
			list.add(tableOfEntries[i].getKey());
		}
	}
	return  list;
	//if its exploding, it may be due to this
	}

	@Override
	public SortedList<V> getValues() {
		// TODO Auto-generated method stub
		CircularSortedDoublyLinkedList<V> list = new CircularSortedDoublyLinkedList<V>(valueComp);
		for (int i = 0; i < tableOfEntries.length; i++) {
			if(!isAvailable[i])
			{
				list.add(tableOfEntries[i].getValue());
			}
		}
		return  list;
	}

	public void reallocate()
	{
		if(this.currentSize == this.tableOfEntries.length)
		{
			HashTableOA<K, V> newHash = new HashTableOA<K, V>(this.currentSize + 97,keyComp, valueComp);
			MapEntry<K,V>[] tableOfEntriesButLarger = newHash.tableOfEntries;
			Boolean[] isAvailableButLarger = newHash.isAvailable;
			
			
			MapEntry<K,V>[] copyTableEntries = (MapEntry<K, V>[]) new Object[this.tableOfEntries.length]; ;
			for (int i = 0; i < copyTableEntries.length; i++) 
			{
				copyTableEntries[i] = this.tableOfEntries[i];
			}
			

			this.tableOfEntries = tableOfEntriesButLarger;
			this.isAvailable = isAvailableButLarger;
			
			for (int i = 0; i < copyTableEntries.length; i++) 
			{
				this.put(copyTableEntries[i].getKey(), copyTableEntries[i].getValue());
			}
			
		}
	}
	public void clear()
	{
		for (int i = 0; i < this.tableOfEntries.length; i++) {
			this.tableOfEntries[i] = null;
			this.isAvailable[i] = true;
		}
		
		this.currentSize = 0;
	}
}
