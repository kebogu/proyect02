package edu.uprm.cse.datastructures.cardealer;
//
import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarTable;
import edu.uprm.cse.datastructures.cardealer.util.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;
import edu.uprm.cse.datastructures.cardealer.util.Map;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

import java.awt.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
//
@Path("/cars")
//this class contains methods that when executed with maven they allow you to add cars, get them, get all of them, make changes to added cars and delete added cars
public class CarManager {
	
	private Map<Long, Car> targetCars = CarTable.getInstance(); 
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCar() {
		//we will use this instance of the CSDLL (you will see this in every method)
	 //CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance().getList(); 
	 
	 
	 
		//getting it to an array is hard due to datatypes, so i used two compatible arrays to get it to work
	 
	 Car[] arr = new Car[targetCars.size()];
	 SortedList<Car> newList =  targetCars.getValues();
	 
	 //here we individually make every element Car type and put it in arr
	 for (int i = 0; i < targetCars.size(); i++) {
		arr[i] =  newList.get(i);
	}
	
	return  arr;
	}
	
	 @GET
	  @Path("{id}")
	  @Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		// CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance().getList();
		///basic for loop going through the list, checking if the id matches, if it does, we return it
		if(this.targetCars.contains(id))
		{
			return this.targetCars.get(id);
		}
		else 
		{
			throw new WebApplicationException(404);
		}
		
		
	  }

	  @POST
	    @Path("/add")
	    @Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
		  
//		 CSDLL has an add method, we use it to add the new car
//			
//		
//		  CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance().getList();
	if(this.targetCars.contains(car.getCarId()))
	  {
		  return Response.status(409).build();
	  }
	else
	  {
		  this.targetCars.put(car.getCarId(), car);
	      return Response.status(201).build();
	  }
	    }
	 
	  @PUT
	    @Path("{id}/update")
	    @Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car, @PathParam("id") long id){
//  CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance().getList();
//we use the foundation of the get car class and simply remove and add at the appropriate space

//		  for (int i = 0; i < cList.size(); i++) {
//				if(cList.get(i).getCarId() == car.getCarId())
//				{
//					cList.remove(i);
//					cList.add(car);
		  if(this.targetCars.contains(id))
			{
				//this.targetCars.remove(id);
				this.targetCars.put(id, car);
				return Response.status(Response.Status.OK).build();
			}
		  
		 else  return Response.status(Response.Status.NOT_FOUND).build();
	   }
	
	  @DELETE
	    @Path("{id}/delete")
	 public Response deleteCar(@PathParam("id") long id){
		 // CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance().getList();
		  //simpler version of the updateCar class
//		  for (int i = 0; i < cList.size(); i++) {
//				if(cList.get(i).getCarId() == id)
//				{
//					cList.remove(i);
//					return Response.status(Response.Status.OK).build();
//				}
//			}
		  if(this.targetCars.contains(id))
		  {
			  this.targetCars.remove(id);
			  return Response.status(Response.Status.OK).build();
			  
		  }
		  
		  else throw new WebApplicationException(404);
	      }
	    }

