package edu.uprm.cse.datastructures.cardealer;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Comparator;

import org.junit.Test;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;
import edu.uprm.cse.datastructures.cardealer.util.Map;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

// Comparator for integers


public class HashTableTest {
	@Test
	public void testIsEmpty() {
		Map<Long,Long> list = new HashTableOA<Long,Long>();
		assertTrue("List should be empty", list.isEmpty());
 		list.put(new Long(5), new Long(5));
		assertFalse(list.isEmpty());
	}
	@Test
	public void testSizeAddGetContains() {
		Long[] results = {new Long(1),new Long(2),new Long(5)};
		Map<Long,Long> list = new HashTableOA<Long,Long>();
		list.put(new Long(1),new Long(1));
		list.put(new Long(2),new Long(2));
		list.put(new Long(5),new Long(5));
		assertEquals("Should have size of 3",3, list.size());
		Long[] listIn = new Long[3];
		
		Long five = list.get(new Long(5));
		Long one = list.get(new Long(1));
		Long Null = list.get(new Long(3));
		
		assertEquals("should be 5", five, new Long(5));
		assertEquals("should be 1", one, new Long(1));
		assertEquals("should be null", Null, null);
		assertTrue("should contain 2", list.contains(new Long(2)));
		

		
	}
	@Test
	public void testRemoveObject() {
		Map<Long,Long> list = new HashTableOA<Long,Long>();
		
		Long one = new Long(1);
		Long two = new Long(2);
		Long five = new Long(5);
		
		list.put(one,one);
		list.put(two,two);
		list.put(five,five);
		assertEquals("Should have size of 3",3, list.size());
		Long assertTwo = list.remove(two);
		
		assertEquals("Size should be 2 after remove()", 2, list.size());
		assertEquals("Remove result should be two", assertTwo, two);
		assertEquals("Should equal null" ,list.get(two), null);
		
		assertEquals("Should not find elements to remove and return null", list.remove(new Long(7)), null);
		assertEquals("Size should still be 2", 2, list.size());

		list.remove(five);
		assertEquals("list size should be 1" , list.size(), 1);
		
		list.remove(one);
		
		assertTrue("List should now be empty", list.isEmpty());
		
	}
	
	@Test
	public void testGetKeys() {
		Map<Integer,Integer> list = new HashTableOA<Integer,Integer>(11,new IntegerComparator(),new IntegerComparator());
		
		SortedList<Integer> result = new CircularSortedDoublyLinkedList<Integer>(new IntegerComparator());
		
		result.add(1);
		result.add(5);
		result.add(2);
		
		Integer one = new Integer(1);
		Integer two = new Integer(2);
		Integer five = new Integer(5);
		
		list.put(one,one);
		list.put(five,five);
		list.put(two,two);
		
		SortedList<Integer> getKeys =  list.getKeys();
		
		assertEquals("These two buckets should be equal",result.get(0),getKeys.get(0));
		assertEquals("These two buckets should be equal",result.get(1),getKeys.get(1));
		assertEquals("These two buckets should be equal",result.get(2),getKeys.get(2));

		
		result.remove(0);
		
		list.remove(1);
		
		getKeys = (SortedList<Integer>) list.getKeys();
		
		assertEquals("Still should be equal", result.get(0),getKeys.get(0));
		assertEquals("These two buckets should be equal",getKeys.get(1),result.get(1));

		list.remove(1);
		
		assertFalse("Should not be equal", result.equals(getKeys));
		
		getKeys = (SortedList<Integer>) list.getKeys();
		
		assertFalse("Does not contain 1", list.contains(one));
		
		
		
		
		
		
		
		
	}
	
	@Test
	public void testGetValues() {
		Map<Integer,Integer> list = new HashTableOA<Integer,Integer>(11,new IntegerComparator(),new IntegerComparator());
		
		SortedList<Integer> result = new CircularSortedDoublyLinkedList<Integer>(new IntegerComparator());
		
		result.add(1);
		result.add(5);
		result.add(2);
		
		Integer one = new Integer(1);
		Integer two = new Integer(2);
		Integer five = new Integer(5);
		
		list.put(one,one);
		list.put(five,five);
		list.put(two,two);
		
		SortedList<Integer> getValues =  list.getValues();
		
		assertEquals("These two lists should be equal",result.get(0),getValues.get(0));
		assertEquals("These two lists should be equal",result.get(1),getValues.get(1));
		assertEquals("These two lists should be equal",result.get(2),getValues.get(2));

		
		result.remove(0);
		
		list.remove(1);
		
		getValues = list.getValues();
		
		assertEquals("Still should be equal", getValues.get(0),result.get(0));
		assertEquals("Still should be equal", getValues.get(1),result.get(1));

		
		list.remove(1);
		
		assertFalse("Should not be equal", result.equals(getValues));
		
		getValues = list.getValues();
		
		assertFalse("Does not contain 1", list.contains(one));
		
	}
//	@Test
//	public void testRemoveIndex() {
//		Integer[] results = {1,5};
//		SortedList<Integer> list = new HashTableOA<Integer>(new IntegerComparator());
//		list.add(1);
//		list.add(2);
//		list.add(5);
//		assertEquals("Should have size of 3",3, list.size());
//		assertTrue("Should remove element 2 (index 1)", list.remove(1));
//		assertEquals("Size should be 2 after remove()", 2, list.size());
//		Integer[] listIn = new Integer[2];
//		for (int i = 0; i < list.size(); i++) {
//			listIn[i] = list.get(i);
//		}
//		assertArrayEquals("List should be [1,5]", results, listIn);
//		assertTrue("Should remove element in index 0", list.remove(0));
//		assertEquals("Size should now be 1", 1, list.size());
//		assertEquals("List should now only have elemnt 5", (Integer) 5, (Integer) list.get(0));
//		assertTrue("Should remove last element in list", list.remove(0));
//		assertTrue("List should now be empty", list.isEmpty());
//		
//		assertThrows(IndexOutOfBoundsException.class, () -> {list.remove(7);}, "Should return false if it cannot find the element");
//	}
//	@Test
//	public void testRemoveAll() {
//		SortedList<Integer> list = new HashTableOA<Integer>(new IntegerComparator());
//		list.add(1);
//		list.add(2);
//		list.add(5);
//		list.add(5);
//		list.add(2);
//		list.add(5);
//		assertEquals("Should remove 3 fives", 3, list.removeAll(5));
//		assertEquals("Size should now be 3", 3, list.size());
//		
//	}
//	@Test
//	public void testFirst() {
//		SortedList<Integer> list = new HashTableOA<Integer>(new IntegerComparator());
//		list.add(1);
//		list.add(2);
//		list.add(5);
//		assertEquals("Should get first element 1", (Integer) 1, list.first());
//		
//	}
//	@Test
//	public void testLast() {
//		SortedList<Integer> list = new HashTableOA<Integer>(new IntegerComparator());
//		list.add(1);
//		list.add(2);
//		list.add(5);
//		assertEquals("Should get last element 5", (Integer) 5, list.last());
//		
//	}
//	@Test
//	public void testClear() {
//		SortedList<Integer> list = new HashTableOA<Integer>(new IntegerComparator());
//		list.add(1);
//		list.add(2);
//		list.add(5);
//		assertEquals("Should have size 3", 3, list.size());
//		list.clear();
//		assertTrue("List should be empty:", list.isEmpty());
//	}
//	@Test
//	public void testContains() {
//		SortedList<Integer> list = new HashTableOA<Integer>(new IntegerComparator());
//		list.add(1);
//		list.add(2);
//		list.add(5);
//		assertTrue("Should contain element 5", list.contains(5));
//		assertFalse("Should not contain element 10", list.contains(10));
//		
//	}
//	@Test
//	public void testFirstIndex() {
//		SortedList<Integer> list = new HashTableOA<Integer>(new IntegerComparator());
//		list.add(1);
//		list.add(2);
//		list.add(5);
//		list.add(2);
//		list.add(2);
//		assertEquals("Should get first index of 2 which is 1", 1, list.firstIndex(2));
//		assertEquals("Should get first index of 5 which is 4", 4, list.firstIndex(5));
//		assertEquals("Should return -1 if not found", -1, list.firstIndex(20));
//		
//	}
//	@Test
//	public void testLastIndex() {
//		SortedList<Integer> list = new HashTableOA<Integer>(new IntegerComparator());
//		list.add(1);
//		list.add(2);
//		list.add(5);
//		list.add(2);
//		list.add(2);
//		assertEquals("Should get last index of 2 which is 3", 3, list.lastIndex(2));
//		assertEquals("Should get last index of 5 which is 4", 4, list.lastIndex(5));
//		assertEquals("Should return -1 if not found", -1, list.lastIndex(20));
//		
//	}
	/*@Test
	public void testIterator() {
		SortedList<Integer> list = new HashTableOA<Integer>(new IntegerComparator());
		list.add(1);
		list.add(2);
		list.add(5);
		list.add(9);
		list.add(10);
		int i=0;
		Integer[] expected = {1,2,5,9,10};
		Integer[] listIn = new Integer[5];
		for(Integer y: list)
		{
			listIn[i++] = y;
		}
		assertArrayEquals("After iteration lists should be the same", expected, listIn);
		
	}
	@Test
	public void testIteratorIndex() {
		SortedList<Integer> list = new HashTableOA<Integer>(new IntegerComparator());
		list.add(1);
		list.add(2);
		list.add(5);
		list.add(9);
		list.add(10);
		int i=0;
		Integer[] expected = {5,9,10};
		Integer[] listIn = new Integer[3];
		for (Iterator<Integer> iter = list.iterator(2); iter.hasNext(); )
		{
			listIn[i++] = iter.next();
		}
		assertArrayEquals("After iteration lists should be the same", expected, listIn);
		
	}
	@Test
	public void testReverseIterator() {
		SortedList<Integer> list = new HashTableOA<Integer>(new IntegerComparator());
		list.add(1);
		list.add(2);
		list.add(5);
		list.add(9);
		list.add(10);
		int i=0;
		Integer[] expected = {10,9,5,2,1};
		Integer[] listIn = new Integer[5];
		for (ReverseIterator<Integer> iter = list.reverseIterator(); iter.hasPrevious(); ){
			listIn[i++] = iter.previous();
		}
		assertArrayEquals("After iteration lists should be the same", expected, listIn);
		
	}
	@Test
	public void testReverseIteratorIndex() {
		SortedList<Integer> list = new HashTableOA<Integer>(new IntegerComparator());
		list.add(1);
		list.add(2);
		list.add(5);
		list.add(9);
		list.add(10);
		int i=0;
		Integer[] expected = {5,2,1};
		Integer[] listIn = new Integer[3];
		for (ReverseIterator<Integer> iter = list.reverseIterator(2); iter.hasPrevious(); ){
			listIn[i++] = iter.previous();
		}
		assertArrayEquals("After iteration lists should be the same", expected, listIn);
		
	}*/

}


